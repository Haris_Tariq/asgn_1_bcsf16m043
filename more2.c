#include <stdio.h>
#include <stdlib.h>

#define	PAGELEN	20
#define	LINELEN	512

void do_more(FILE *);
int  get_input();
int main(int argc , char *argv[])
{
   int i=0;
   if (argc == 1){
       printf("This is the help page\n");
       exit (0);
   }
   FILE * fp;
   while(++i < argc){
      fp = fopen(argv[i] , "r");
      if (fp == NULL){
         perror("Can't open file");
         exit (1);
      }
      do_more(fp);
      fclose(fp);
   }  
   return 0;
}

void do_more(FILE *fp)
{
   int num_of_lines = 0;
   int rv;
   char buffer[LINELEN];
   while (fgets(buffer, LINELEN, fp)){
      fputs(buffer, stdout);
      num_of_lines++;
      if (num_of_lines == PAGELEN) //checking page lenght
      {
         rv = get_input();		
         if (rv == 0)//user pressed q
            break;// break
         else if (rv == 1)//user pressed space bar
            num_of_lines = num_of_lines - PAGELEN;
         else if (rv == 2)//user pressed enter
	    num_of_lines = num_of_lines - 1; //show one more line
         else if (rv == 3) //invalid character
            break; 
      }
  }
}

int get_input()
{
   int ch;		
     ch=getchar();
      if(ch == 'q')
	 return 0; // Quit
      if (ch == ' ' )			
	 return 1; // Space bar
      if (ch == '\n' )	
	 return 2; // end of line
      return 3;    // else
   return 0;
}

